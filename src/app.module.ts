import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

// Librerias
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { UsuarioModule } from './usuario/usuario.module';

// Configuraciones
import { dbdatasource }  from './ormconfig'
import { LoggerMiddleware } from './middleware/logger.middleware';
// import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';
import { RolModule } from './rol/rol.module';
import { AsignacionRolModule } from './asignacion-rol/asignacion-rol.module';

@Module({
  imports: [
    TypeOrmModule.forRoot( dbdatasource ),
    UsuarioModule,
    AuthModule,
    RolModule,
    AsignacionRolModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule{
  constructor(private dataSource: DataSource) {}
  configure(consumer: MiddlewareConsumer){
    consumer.apply(LoggerMiddleware).forRoutes('*');
  }
}
