import { Test, TestingModule } from '@nestjs/testing';
import { AsignacionRolController } from './asignacion-rol.controller';
import { AsignacionRolService } from './asignacion-rol.service';
import { CreateAsignacionRolDto } from './dto/create-asignacion-rol.dto';

describe('AsignacionRolController', () => {
  let controller: AsignacionRolController;
  let service: AsignacionRolService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AsignacionRolController],
      providers: [
        {
          provide: AsignacionRolService,
          useFactory: () => ({
            create: jest.fn()
          })
        }
      ],
    }).compile();

    controller = module.get<AsignacionRolController>(AsignacionRolController);
    service = module.get<AsignacionRolService>(AsignacionRolService);
  });

  it('should be create a new user with rol', async () => {
    const asignacion = new CreateAsignacionRolDto()
    await controller.create(asignacion)
    expect(service.create).toHaveBeenLastCalledWith(asignacion);
  });
});
