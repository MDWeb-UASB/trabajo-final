import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { AsignacionRolService } from './asignacion-rol.service';
import { CreateAsignacionRolDto, UpdateAsignacionRolDto, GetIdAsignacionRolDto, GetByIdUsuario } from './dto/index.dto';
import { RolesGuard, JwtStrategy, JwtauthGuard } from '../auth/guards/index.guard'
import { ApiTags } from '@nestjs/swagger';
@ApiTags('Asignacion Cargo Controller')
// @UseGuards(RolesGuard, JwtStrategy, JwtauthGuard)
@Controller('asignacion-rol')
export class AsignacionRolController {
  constructor(private readonly asignacionRolService: AsignacionRolService) {}

  @Post()
  create(@Body() createAsignacionRolDto: CreateAsignacionRolDto) {
    return this.asignacionRolService.create(createAsignacionRolDto);
  }

  @Get()
  findAll() {
    return this.asignacionRolService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.asignacionRolService.findOne(+id);
  }

  @Get('/getAsginacionUsuarioById/:id')
  async findBtUsuarioId(@Param('id') id: number, @Body() getByIdUsuario: GetByIdUsuario ){
    const data = await this.asignacionRolService.findBtUsuarioId(getByIdUsuario)
    console.log("encontrado:", {data})
    return data;
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateAsignacionRolDto: UpdateAsignacionRolDto) {
    return this.asignacionRolService.update(+id, updateAsignacionRolDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.asignacionRolService.remove(+id);
  }
}
