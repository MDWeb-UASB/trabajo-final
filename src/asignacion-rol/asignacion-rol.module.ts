import { Module } from '@nestjs/common';
import { AsignacionRolService } from './asignacion-rol.service';
import { AsignacionRolController } from './asignacion-rol.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AsignacionRol, Rol, Usuario } from '../entities/index';
import { AsignacionRepository } from './repository/asignacion-rol.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([Usuario, Rol, AsignacionRol]),
  ],
  controllers: [AsignacionRolController],
  providers: [AsignacionRolService, AsignacionRepository],
})
export class AsignacionRolModule {}
