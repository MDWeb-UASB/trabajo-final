import { Test, TestingModule } from '@nestjs/testing';
import { AsignacionRolService } from './asignacion-rol.service';
import { AsignacionRepository } from './repository/asignacion-rol.repository';

describe('AsignacionRolService', () => {
  let service: AsignacionRolService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AsignacionRolService,
        {
          provide: AsignacionRepository,
          useValue: {
            getAll: jest.fn(),
            create: jest.fn(),
          },
        }
      ],
    }).compile();

    service = module.get<AsignacionRolService>(AsignacionRolService);
  });

  it('debe ser definido', () => {
    expect(service).toBeDefined();
  });
});
