import { Injectable } from '@nestjs/common';
import { CreateAsignacionRolDto, UpdateAsignacionRolDto, GetIdAsignacionRolDto, GetByIdUsuario } from './dto/index.dto';
import { AsignacionRepository } from './repository/asignacion-rol.repository';

@Injectable()
export class AsignacionRolService {
  constructor(
    private readonly asignacionRepository: AsignacionRepository
  ){}

  create(createAsignacionRolDto: CreateAsignacionRolDto) {
    return this.asignacionRepository.crear(createAsignacionRolDto);
  }

  findAll() {
    return  this.asignacionRepository.listar();
  }

  findOne(id: number) {
    return this.asignacionRepository.listar();
  }
  findBtUsuarioId(getByIdUsuario: GetByIdUsuario) {
    return this.asignacionRepository.listarByUsuario(getByIdUsuario)
  }

  update(id: number, updateAsignacionRolDto: UpdateAsignacionRolDto) {
    return `This action updates a #${id} asignacionRol`;
  }

  remove(id: number) {
    return `This action removes a #${id} asignacionRol`;
  }
}
