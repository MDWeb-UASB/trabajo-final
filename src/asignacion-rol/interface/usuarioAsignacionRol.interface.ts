export interface UsuarioConRolesDTO {
    id: number;
    nombre: string;
    nombreUsuario: string;
    email: string;
    asignacionesRoles: AsignacionRolDTO[];
}

export interface AsignacionRolDTO {
    id: number;
    rol: RolDTO;
}

export interface RolDTO {
    id: number;
    nombre: string;
    descripcion: string;
}