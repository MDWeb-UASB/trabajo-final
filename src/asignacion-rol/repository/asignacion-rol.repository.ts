import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { Injectable, InternalServerErrorException, NotFoundException } from "@nestjs/common";
import { AsignacionRol, Rol, Usuario } from "../../entities/index";
import { CreateAsignacionRolDto, UpdateAsignacionRolDto, GetIdAsignacionRolDto, GetByIdUsuario } from '../dto/index.dto';
import { UsuarioConRolesDTO, AsignacionRolDTO } from '../interface/usuarioAsignacionRol.interface'
@Injectable()
export class AsignacionRepository {
    constructor(
        @InjectRepository(Usuario)
        private readonly usuarioRepository: Repository<Usuario>,
        @InjectRepository(Rol)
        private readonly rolRepository: Repository<Rol>,
        @InjectRepository(AsignacionRol)
        private readonly asignacionRolRepository: Repository<AsignacionRol>
    ) { }

    async crear(createAsignacionRolDto: CreateAsignacionRolDto) {
        try {
            const a_usuarios = await this.usuarioRepository.findOne({ where: { id: createAsignacionRolDto.usuario_id } })
            const a_roles = await this.rolRepository.findOne({ where: { id: createAsignacionRolDto.rol_id } })
            if ((!a_usuarios) && (!a_roles)) { throw new NotFoundException(`Error al registrar`) }
            const data = new AsignacionRol()
            data.rol = a_roles
            data.usuario = a_usuarios
            return this.asignacionRolRepository.save(data);
        } catch (error) {
            throw new InternalServerErrorException(error.message)
        }
    }
    async actaulizar(id: number, updateAsignacionRolDto: UpdateAsignacionRolDto) {
        try {
            const data = await this.asignacionRolRepository.findOneBy({ id });
            if (!data) {
                throw new NotFoundException('No se encontró la información');
            }

            const usuario = await this.usuarioRepository.findOneBy({ id: updateAsignacionRolDto.usuario_id });
            const rol = await this.rolRepository.findOneBy({ id: updateAsignacionRolDto.rol_id });

            if (!usuario || !rol) {
                throw new NotFoundException('Error al modificar');
            }

            data.usuario = usuario;
            data.rol = rol;

            return this.asignacionRolRepository.save(data);
        } catch (error) {
            throw new InternalServerErrorException(error.message);
        }
    }
    async listar() {
        try {
            const data = await this.asignacionRolRepository.find({
                relations: ['usuario', 'rol'],
                where: {
                    usuario: {
                        deleteAt: null
                    },
                    rol: {
                        deleteAt: null
                    },
                    deleteAt: null
                }
            });
            if (!data) { throw new NotFoundException("No se encontraron Datos") }
            return data;
        } catch (error) {
            throw new InternalServerErrorException(error.message)
        }
    }

    async listarByUsuario(getByIdUsuario: GetByIdUsuario) {
        try {
            const { id } = getByIdUsuario
            const info: UsuarioConRolesDTO[] = await this.usuarioRepository.find({
                relations: ['asignacionesRoles', 'asignacionesRoles.rol'],
                where: {
                    id,
                    deleteAt: null
                }
            })
            console.log("informacion: ",info)
            // // Tentativa 1
            // const data: UsuarioConRolesDTO[] = info.map(usuario => ({
            //     id: usuario.id,
            //     nombre: usuario.nombre,
            //     nombreUsuario: usuario.nombreUsuario,
            //     email: usuario.email,
            //     asignacionesRoles: usuario.asignacionesRoles.map(asignacionRol => ({
            //         id: asignacionRol.id,
            //         rol: {
            //             id: asignacionRol.rol?.id,
            //             nombre: asignacionRol.rol?.nombre,
            //             descripcion: asignacionRol.rol?.descripcion
            //         }
            //     }))
            // }));
            // console.log('Usuarios con roles:', {data});
            // return data
            // // Tentativa 2
            // let data: UsuarioConRolesDTO[] = []
            // info.forEach(element => {
            //     const a_u: UsuarioConRolesDTO = {
            //         id: element.id,
            //         nombre: element.nombre,
            //         nombreUsuario: element.nombreUsuario,
            //         email: element.email,
            //         asignacionesRoles: element.asignacionesRoles.map( (d) => {
            //             if (d.rol) {
            //                 const asignacionRol: AsignacionRolDTO = {
            //                     id: d.id,
            //                     rol: {
            //                         id: d.rol.id,
            //                         nombre: d.rol.nombre,
            //                         descripcion: d.rol.descripcion
            //                     }
            //                 }
            //                 return asignacionRol;
            //             }
            //         }).filter(Boolean)
            //     }
            //     data.push(a_u);
            // });
            // console.log('Usuarios con roles:', {data});
            // return data
            return info
        } catch (error) {
            throw new InternalServerErrorException(error.message)
        }
    }

}