import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { UsuarioService } from '../usuario/usuario.service';
import { create } from 'domain';

describe('AuthController', () => {
  let controller: AuthController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        {
          provide: AuthService,
          useValue: {
            create: jest.fn()
          },
        },
        {
          provide: UsuarioService,
          useValue: {
            create: jest.fn()
          },
        },
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
  });

  it('debe ser definido', () => {
    expect(controller).toBeDefined();
  });
});
