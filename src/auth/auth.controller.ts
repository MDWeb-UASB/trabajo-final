import { Body, Controller, Post } from '@nestjs/common';
import { VerificarDto } from '../usuario/dto/verificar.dto';
import { UsuarioService } from '../usuario/usuario.service';
import { AuthService } from './auth.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Acceso Controller')
@Controller('auth')
export class AuthController {
    constructor(
        private readonly authService: AuthService,
        private usuarioService: UsuarioService
    ){}

    @Post('login')
    async login(@Body() verificarDto: VerificarDto) {
        const usuario = await this.usuarioService.verificarCuenta(verificarDto);
        return await this.authService.login(usuario);
    }
}
