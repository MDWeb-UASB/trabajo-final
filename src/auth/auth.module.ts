import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UsuarioModule } from '../usuario/usuario.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Usuario } from '../entities';
import { JwtModule } from '@nestjs/jwt';
import { UsuarioService } from '../usuario/usuario.service';
import { UsuarioRepository } from '../usuario/repository/usuario.repository';
import { JwtStrategy, JwtauthGuard } from './guards/index.guard';
import * as dotenv from 'dotenv';
import { HttpModule, HttpService } from '@nestjs/axios';
dotenv.config();

@Module({
  imports: [ 
    JwtModule.register({
      global: true,
      secret: process.env.JWT_SEED,
      signOptions: { expiresIn: '6h' },
    }),
    TypeOrmModule.forFeature([Usuario]),
    UsuarioModule,
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),
   ],
  providers: [AuthService, UsuarioService, UsuarioRepository, JwtStrategy, JwtauthGuard],
  controllers: [AuthController],
  exports: [AuthModule]
})
export class AuthModule {}
