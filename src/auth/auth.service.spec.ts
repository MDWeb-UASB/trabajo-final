import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { UsuarioService } from '../usuario/usuario.service';
import { UsuarioRepository } from '../usuario/repository/usuario.repository';
import { JwtStrategy } from './guards/jwt_strategy.guard';
import { JwtauthGuard } from './guards/jwt_guard.guard';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { HttpService } from '@nestjs/axios';

describe('AuthService', () => {
  let service: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        JwtModule.register({
          secret: 'test',
          signOptions: { expiresIn: '60s' },
        }),
      ],
      providers: [
        AuthService,
        {
          provide: UsuarioService,
          useValue: {
            create: jest.fn()
          },
        },
        {
          provide: UsuarioRepository,
          useValue: {
            findByEmail: jest.fn()
          }
        },
        {
          provide: JwtStrategy,
          useValue: {
            validate: jest.fn(),
          },
        },
        JwtauthGuard,
        JwtService,
        // provide a mock implementation of HttpService
        {
          provide: HttpService,
          useValue: {
            create: jest.fn()
          },
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
  });

  it('Debe ser definido', () => {
    expect(service).toBeDefined();
  });
});