import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Usuario } from '../entities/index';

@Injectable()
export class AuthService {
    constructor(private jwtService: JwtService){}
    async login(usuario:Usuario) {
        const payload = {email: usuario.email, sub: usuario.id};
        return {
            acces_token: this.jwtService.sign(payload),
            // user: usuario.nombreUsuario
        }
    }
}
