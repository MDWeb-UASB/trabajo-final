import { Injectable, CanActivate, ExecutionContext, InternalServerErrorException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Usuario } from '../../entities/index';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(
    private reflector: Reflector,
    private jwtService: JwtService,
    @InjectRepository(Usuario)
    private usuarioRepository: Repository<Usuario>,
  ) {}

  async canActivate(
    context: ExecutionContext,
  ): Promise<boolean> {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    if (!roles) {
      return true;
    }
    
    const request = context.switchToHttp().getRequest();
    const token = request.headers.authorization.split(' ')[1];
    
    // Decodificar el token para obtener la información del usuario
    const decodedToken = this.jwtService.decode(token);
    
    // Buscar los roles del usuario en la base de datos
    const usuario: any = await this.usuarioRepository.findOneBy({ id:decodedToken.id });

    // Verificar si el usuario existe
    if (!usuario) {
      console.log('Usuario no encontrado');
      return false;
    }

    const usuarioRoles = usuario.asignacionesRoles.map(asignacionRol => asignacionRol.rol.nombre);

    // Imprimir los roles en la consola
    console.log('Roles del usuario:', usuarioRoles);
    
    return usuarioRoles.some(role => roles.includes(role));
  }
}
