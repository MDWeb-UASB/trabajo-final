import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

import * as dotenv from 'dotenv';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
dotenv.config();

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  const options = new DocumentBuilder()
  .setTitle('Todo API')
  .setDescription('Para los end points del sistema')
  .setVersion('1.0')
  .addServer('http://localhost:3000')
  .addTag('Modulo 3')
  .addBearerAuth({
    description: 'JWT Para autenticacion dentro de el backend',
    type: 'http',
    scheme: 'bearer',
    bearerFormat: 'JWT'
  })
  .build()
  const  document = SwaggerModule.createDocument(app, options)
  SwaggerModule.setup('api-docs', app, document)
  await app.listen(3000);
}
bootstrap();
