import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";

export class CreateRolDto {
    @ApiProperty({
        example: 'admin',
        required: true
    })
    @IsString()
    @IsNotEmpty()
    nombre: string;

    @ApiProperty({
        example: 'Administra el sistema',
        required: true
    })
    @IsString()
    @IsNotEmpty()
    descripcion: string;

}
