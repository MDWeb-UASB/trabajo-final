import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateRolDto } from './create-rol.dto';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class UpdateRolDto extends PartialType(CreateRolDto) {
    @ApiProperty({
        example: '1',
        required: true
    })
    @IsNumber()
    @IsNotEmpty()
    id: number;
    
    @ApiProperty({
        example: 'administrador',
        required: true
    })
    @IsString()
    @IsNotEmpty()
    nombre: string;

    @ApiProperty({
        example: 'administrador del sistema Backend',
        required: true
    })
    @IsString()
    @IsNotEmpty()
    descripcion: string;
}
