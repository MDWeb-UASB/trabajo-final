import { Injectable, InternalServerErrorException, NotFoundException } from "@nestjs/common";
import { CreateRolDto, UpdateRolDto, GetByIdDto } from '../dto/index.dto';
import { Rol } from '../entities/rol.entity';
import { Like, Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { PaginationDto } from "../../common/dto/index.dto";

@Injectable()
export class RolRepository {
    constructor(
        @InjectRepository(Rol)
        private readonly rolRepository: Repository<Rol>
    ) { }
    async crear(createUsuarioDto: CreateRolDto) {
        try {
            const data = await this.rolRepository.save(createUsuarioDto);
            if (!data) { throw new NotFoundException(`Error al registrar`) }
            return data
        } catch (error) {
            throw new InternalServerErrorException(error.message)
        }
    }
    async actualizar(id: number, updateUsuarioDto: UpdateRolDto) {
        try {
            const find = await this.rolRepository.find({ where: { id } })
            if (!find) { throw new NotFoundException(`No se encontro nada con el id: ${id}`) }
            return await this.rolRepository.update(id, updateUsuarioDto);
        } catch (error) {
            throw new InternalServerErrorException(error.message)
        }
    }

    async listar(paginationDto: PaginationDto) {
        try {
            const { limit = 5, offset = 0, parameter = "" } = paginationDto;
            const data = await this.rolRepository.find({
                where: { 
                    deleteAt: null,
                    nombre: Like(`%${parameter}%`),
                    descripcion: Like(`%${parameter}%`),
                },
                take: limit,
                skip: offset,
            });
            if (!data) { throw new NotFoundException(`No se encontro informacion`) }
            return data
        } catch (error) {
            throw new InternalServerErrorException(error.message)
        }
    }

    async findById(getByIdDto: GetByIdDto) {
        try {
            const data = await this.rolRepository.find({ where: { id: getByIdDto.id } })
            if (!data) { throw new NotFoundException(`No se encontro nada con el id: ${getByIdDto.id}`) }
            return data
        } catch (error) {
            throw new InternalServerErrorException(error.message)
        }
    }

    async eliminar(id: number) {
        try {
            const data = await this.rolRepository.findOneBy({ id });
            if (!data) { throw new NotFoundException(`No se encontro nada con el id: ${id}`) }
            data.deleteAt = new Date();
            await this.rolRepository.save(data)
        } catch (error) {
            throw new InternalServerErrorException(error.message);
        }
    }

    async findByName(nombre: string): Promise<Rol> {
        try {
            const data = await this.rolRepository.findOne({ where: { nombre } })
            if (!data) { throw new NotFoundException(`No se encontro nada con el nombre: ${nombre}`) }
            return data
        } catch (error) {
            throw new InternalServerErrorException(error.message)
        }
    }

    async findByDescripcion(descripcion: string): Promise<Rol> {
        try {
            const data = await this.rolRepository.findOne({ where: { descripcion } })
            if (!data) { throw new NotFoundException(`No se encontro nada con el descripcion: ${descripcion}`) }
            return data
        } catch (error) {
            throw new InternalServerErrorException(error.message)
        }
    }
}