import { Test, TestingModule } from '@nestjs/testing';
import { RolController } from './rol.controller';
import { RolService } from './rol.service';
import { create } from 'domain';

describe('RolController', () => {
  let controller: RolController;
  let service: RolService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RolController],
      providers: [
        {
          provide: RolService,
          useValue: {
            create: jest.fn()
          }
        },
        {
          provide: RolService,
          useValue: {
            create: jest.fn()
          }
        }
      ],
    }).compile();

    controller = module.get<RolController>(RolController);
  });

  it('should be create a rol', () => {
    expect(controller).toBeDefined();
  });
});
