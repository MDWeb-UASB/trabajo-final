import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { RolService } from './rol.service';
import { CreateRolDto, UpdateRolDto, GetByIdDto } from './dto/index.dto';
import { ApiBody, ApiResponse, ApiTags } from '@nestjs/swagger';
import { JwtStrategy, JwtauthGuard } from '../auth/guards/index.guard';
import { PaginationDto } from '../common/dto/index.dto'

@ApiTags('Rol Controller')
// @UseGuards(JwtStrategy, JwtauthGuard)
@Controller('rol')
export class RolController {
  constructor(private readonly rolService: RolService) {}

  @ApiResponse({ status: 201, description: 'Se ha creado exitosamente' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 402, description: 'Error al registrar' })
  @ApiResponse({ status: 403, description: 'Usuario no encontrado' })
  @ApiBody({
    type: CreateRolDto,
    description: 'Creando un nuevo rol',
  })
  @Post()
  create(@Body() createRolDto: CreateRolDto) {
    return this.rolService.create(createRolDto);
  }

  @Get()
  findAll(paginationDto: PaginationDto) {
    return this.rolService.findAll(paginationDto);
  }

  @ApiResponse({ status: 402, description: 'Error al buscar' })
  @ApiResponse({ status: 403, description: 'Rol no encontrado' })
  @ApiBody({
    type: GetByIdDto,
    description: 'Buscar Usuario por ID',
  })
  @Get(':id')
  findOne(@Body() getByIdDto: GetByIdDto) {
    return this.rolService.findOne(getByIdDto);
  }

  @ApiResponse({ status: 201, description: 'Se ha modificado exitosamente' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 402, description: 'Error al modificar' })
  @ApiResponse({ status: 403, description: 'Usuario no encontrado' })
  @ApiBody({
    type: UpdateRolDto,
    description: 'Creando un nuevo usuario',
  })
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateRolDto: UpdateRolDto) {
    return this.rolService.update(+id, updateRolDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.rolService.remove(+id);
  }
}
