import { Module } from '@nestjs/common';
import { RolService } from './rol.service';
import { RolController } from './rol.controller';


import { TypeOrmModule } from '@nestjs/typeorm';
import { Rol } from '../entities/index';
import { RolRepository } from './repository/rol.repository';
@Module({
  imports: [
    TypeOrmModule.forFeature([Rol]),
  ],
  controllers: [RolController],
  providers: [RolService, RolRepository],
})
export class RolModule {}
