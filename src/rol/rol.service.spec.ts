import { Test, TestingModule } from '@nestjs/testing';
import { RolService } from './rol.service';
import { Rol } from '../entities/index'
import { RolRepository } from './repository/rol.repository';
describe('RolService', () => {
  let service: RolService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RolService,
        {
          provide : RolRepository ,
          useValue: {
            getAllRoles: jest.fn(),
            createRole: jest.fn()
          }
        }
      ],
    }).compile();

    service = module.get<RolService>(RolService);
  });

  it('debe ser definido', () => {
    expect(service).toBeDefined();
  });
});
