import { Injectable } from '@nestjs/common';
import { CreateRolDto, UpdateRolDto, GetByIdDto } from './dto/index.dto';
import { RolRepository } from './repository/rol.repository';
import { PaginationDto } from '../common/dto/index.dto';

@Injectable()
export class RolService {
  constructor(
    private readonly rolRepository: RolRepository
  ){}
  async create(createRolDto: CreateRolDto) {
    return this.rolRepository.crear(createRolDto)
  }

  findAll(paginationDto: PaginationDto) {
    return this.rolRepository.listar(paginationDto);
  }

  findOne(getByIdDto: GetByIdDto) {
    return this.rolRepository.findById(getByIdDto);
  }

  update(id: number, updateRolDto: UpdateRolDto) {
    return this.rolRepository.actualizar(id, updateRolDto);
  }

  remove(id: number) {
    return this.rolRepository.eliminar(id);
  }
}
