import { ApiProperty } from "@nestjs/swagger";
import { IsAlphanumeric, IsEmail, IsNotEmpty, IsString, MinLength } from "class-validator";

export class CreateUsuarioDto {
    @ApiProperty({
        example: 'Juan Pablo',
        required: true
    })
    @IsString()
    @IsNotEmpty()
    @MinLength(5, { 
        message: 'El nombre debe tener al menos 5 caracteres'})
    nombre: string;

    @ApiProperty({
        example: 'Pablex',
        required: true
    })
    @IsString()
    @IsNotEmpty()
    @MinLength(5, { 
        message: 'El nombre de usuario debe tener al menos 5 caracteres'})
    @IsAlphanumeric(null, { message: 'Solo se permiten numeros y letras' })
    nombreUsuario: string;

    @IsString()
    @IsEmail(null, { message: 'Ingrese un correo valido' })
    @IsNotEmpty()
    @ApiProperty({
        example: 'Pablex@gmail.com',
        required: true
    })
    email: string;

    @ApiProperty({
        example: '123456',
        required: true
    })
    @IsString()
    @IsNotEmpty()
    @MinLength(8, { message: 'La contrasela al menos debe tener 8 caracteres'})
    password: string;
}
