import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, IsString, MinLength } from "class-validator";

export class VerificarDto {
    @ApiProperty({
        example: 'Juan Pablo@gmail.com',
        required: true
    })
    @IsEmail()
    @IsNotEmpty()
    @IsString()
    email: string;

    @ApiProperty({
        example: '123456',
        required: true
    })
    @IsString()
    @IsNotEmpty()
    @MinLength(2, { message: 'Debe de tener al menos dos caracteres'})
    password: string;
}