import { AsignacionRol } from "../../entities/index";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity('usuario')
export class Usuario {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', length: '50', unique: true })
    nombre: string;

    @Column({ type: 'varchar', length: '15', unique: true })
    nombreUsuario: string;

    @Column({ type: 'varchar', length: '50', unique: true })
    email: string;

    @Column({ type: 'varchar' })
    password: string;

    @OneToMany(() => AsignacionRol, (asignacionRol) => asignacionRol.usuario, {
        eager: true,
    })
    asignacionesRoles: AsignacionRol[];

    @CreateDateColumn({
        name: 'created_at',
        type: 'timestamp',
        default: () => 'CURRENT_TIMESTAMP',
    })
    createAt: Date;

    @UpdateDateColumn({
        name: 'update_at',
        type: 'timestamp',
        default: () => 'CURRENT_TIMESTAMP',
    })
    updateAt: Date;

    @DeleteDateColumn({ name: 'delete_at', type: 'timestamp', nullable: true })
    deleteAt: Date;

    constructor(data?: Partial<Usuario>) {
        if (data) Object.assign(this, data);
    }
}
