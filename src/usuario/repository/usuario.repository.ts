import { Injectable, InternalServerErrorException, NotFoundException } from "@nestjs/common";
import { CreateUsuarioDto, UpdateUsuarioDto } from '../dto/index.dto';
import { Usuario } from '../entities/usuario.entity';
import { Like, Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { PaginationDto } from "../../common/dto/index.dto";

@Injectable()
export class UsuarioRepository {
    findOneBy(arg0: { id: any; }) {
      throw new Error('Method not implemented.');
    }
    constructor(
        @InjectRepository(Usuario)
        private readonly usuarioRepository: Repository<Usuario>
    ) { }
    async crear(createUsuarioDto: CreateUsuarioDto) {
        try {
            const data = await this.usuarioRepository.save(createUsuarioDto);
            if (!data) { throw new NotFoundException(`Error al registrar`) }
            return data;
        } catch (error) {
            throw new InternalServerErrorException(error.message)
        }
    }
    async actualizar(id: number, updateUsuarioDto: UpdateUsuarioDto) {
        try {
            const find = await this.usuarioRepository.find({ where: { id } })
            if (!find) { throw new NotFoundException(`No se encontro nada con el id: ${id}`) }
            return this.usuarioRepository.update(id, updateUsuarioDto);
        } catch (error) {
            throw new InternalServerErrorException(error.message)
        }
    }
    async listar(paginationDto: PaginationDto) {
        try {
            const { limit = 5, offset = 0, parameter = "" } = paginationDto;
            const data = await this.usuarioRepository.find({ 
                where: { 
                    deleteAt: null,
                    nombre: Like(`%${parameter}%`),
                    nombreUsuario: Like(`%${parameter}%`)
                }, 
                take: limit,
                skip: offset,
            });
            if (!data) { throw new NotFoundException(`Error no se encontro informacion`) }
            return data;
        } catch (error) {
            throw new InternalServerErrorException(error.message)
        }
    }
    async findById(id: number) {
        try {
            const data = await this.usuarioRepository.findOneBy({ id })
            if (!data) { throw new NotFoundException(`No se encontro nada con el id: ${id}`) }
            return data
        } catch (error) {
            throw new InternalServerErrorException(error.message)
        }
    }
    async eliminar(id: number) {
        try {
            const data = await this.usuarioRepository.findOneBy({ id })
            if (!data) { throw new NotFoundException(`No se encontro nada con el id: ${id}`) }
            data.deleteAt = new Date
            return await this.usuarioRepository.save(data)
        } catch (error) {
            throw new InternalServerErrorException(error.message)
        }
    }

    async findByName(nombre: string): Promise<Usuario> {
        try {
            const data: Usuario = await this.usuarioRepository.findOneBy({ nombre })
            return data
        } catch (error) {
            throw new InternalServerErrorException(error.message)
        }
    }

    async findByEmail(email: string): Promise<Usuario> {
        try {
            const data = await this.usuarioRepository.findOne({ where: { email } })
            if (!data) { throw new NotFoundException(`No se encontro nada con el email: ${email}`) }
            return data
        } catch (error) {
            throw new InternalServerErrorException(error.message)
        }
    }
}