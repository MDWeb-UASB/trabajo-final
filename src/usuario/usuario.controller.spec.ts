import { Test, TestingModule } from '@nestjs/testing';
import { UsuarioController } from './usuario.controller';
import { UsuarioService } from './usuario.service';
import { CreateUsuarioDto } from './dto/create-usuario.dto';

describe('UsuarioController', () => {
  let controller: UsuarioController;
  let service: UsuarioService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsuarioController],
      providers: [
        {
          provide: UsuarioService,
          useFactory: () => ({
            create: jest.fn()
          })
        }
      ],
    }).compile();

    controller = module.get<UsuarioController>(UsuarioController);
    service = module.get<UsuarioService>(UsuarioService);
  });

  it('should be create a user', async () => {
    const usuario = new CreateUsuarioDto()
    await controller.create(usuario)
    expect(service.create).toHaveBeenLastCalledWith(usuario)
  });
});
