import { Test, TestingModule } from '@nestjs/testing';
import { UsuarioService } from './usuario.service';
import { UsuarioRepository } from './repository/usuario.repository';
import { HttpService } from '@nestjs/axios';

describe('UsuarioService', () => {
  let service: UsuarioService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsuarioService,
        {
          provide: UsuarioRepository,
          useValue: {
            getAllUsers: jest.fn(),
            findOneUserByEmail: jest.fn(),
            createUser: jest.fn(),
            updateUser: jest.fn(),
            deleteUser: jest.fn(),
          },
        },
        // provide a mock implementation of HttpService
        {
          provide: HttpService,
          useValue: {
            HttpService
          },
        },
      ],
    }).compile();

    service = module.get<UsuarioService>(UsuarioService);
  });

  it('debe ser definido', () => {
    expect(service).toBeDefined();
  });
});
