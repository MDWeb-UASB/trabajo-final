import { HttpException, HttpStatus, Injectable, InternalServerErrorException } from '@nestjs/common';
import { CreateUsuarioDto, UpdateUsuarioDto, VerificarDto, FindUsuarioDto } from './dto/index.dto';
import { UsuarioRepository } from './repository/usuario.repository';
import { Usuario } from './entities/usuario.entity';
import { HttpService } from '@nestjs/axios';
import { catchError, firstValueFrom } from 'rxjs';
import { AxiosError } from 'axios';
import { PaginationDto } from '../common/dto/index.dto';
@Injectable()
export class UsuarioService {
  constructor(
    private readonly httpService: HttpService,
    private readonly usuarioRepository: UsuarioRepository
  ) { }

  async create(createUsuarioDto: CreateUsuarioDto) {
    const validar = await this.usuarioRepository.findByName(createUsuarioDto.nombre);
    if (validar?.nombre === createUsuarioDto.nombre) { throw new Error(`El usuario con el nombre: ${createUsuarioDto.nombre} ya existe`) };
    return this.usuarioRepository.crear(createUsuarioDto);
  }

  findAll(paginationDto: PaginationDto) {
    return this.usuarioRepository.listar(paginationDto);
  }

  findOne(findUsuarioDto: FindUsuarioDto) {
    const { id } = findUsuarioDto
    return this.usuarioRepository.findById(id);
  }

  update(id: number, updateUsuarioDto: UpdateUsuarioDto) {
    const validacion = this.usuarioRepository.findById(id);
    if (!validacion) {
      throw new Error(`Usuario con el id: ${id} no encontrado`);
    }
    const usuario = this.usuarioRepository.actualizar(id, updateUsuarioDto);
    return usuario;
  }

  remove(id: number) {
    return this.usuarioRepository.eliminar(id);
  }

  async verificarCuenta(verificarDto: VerificarDto): Promise<Usuario> {
    const validar = await this.usuarioRepository.findByEmail(verificarDto.email);
    if (!validar || validar.password !== verificarDto.password) { throw new Error(`El correro: ${verificarDto.email} o la contraseña: ${verificarDto.password} no son correctos!`); }
    return this.usuarioRepository.findByEmail(verificarDto.email);
  }

  async getCountry() {
    try {
      const url = process.env.URL_API+'all';
      const headers = {
        'Content-Type': 'application/json'
      };
      const { data }: any = await firstValueFrom(
        this.httpService.get<any>(url, {headers}).pipe(
          catchError(async (error: AxiosError) => {
            console.log(error);
            throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
          }),
        ),
      );
      return data
    } catch (error) {
      throw new InternalServerErrorException(error.message)
    }
  }

  async getCountryById(id: number) {
    try {
      const url = process.env.URL_API + `/${id}`;
      const headers = {
        'Content-Type': 'application/json'
      };
      const { data }: any = await firstValueFrom(
        this.httpService.get<any>(url, {headers}).pipe(
          catchError(async (error: AxiosError) => {
            console.log(error);
            throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
          }),
        ),
      );
      return data
    } catch (error) {
      throw new InternalServerErrorException(error.message)
    }
  }
}
